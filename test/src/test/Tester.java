package test;

import java.util.Scanner;

class Tester {

	static boolean isPrime(int n) {
		// check if n is a multiple of 2
		if (n % 2 == 0)
			return false;
		// if not, then just check the odds
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

	public static void main(String[] args) {

		

		
		 System.out.println("Enter a number: ");
		 Scanner sc = new Scanner(System.in);
		 int i = sc.nextInt();


		for (int z = 0; z < i; z++) {
			++i;

			if (isPrime(i) == true) {
				System.out.println("The next prime number is " + i);
				break;
			 }
		 }



	}



}
